export class Synonym {
    word: String;
    score: String;
    tags: String[];
}