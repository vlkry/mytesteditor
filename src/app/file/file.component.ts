import { Component, OnInit } from '@angular/core';
import { TextService } from '../text-service/text.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  text = '';
  words: String[] = [];
  synonyms: String[] = [];
  punctiationMarks: String[] = [",", ".", "!", "?"];

  constructor(private textService: TextService) {
  }

  ngOnInit() {
    this.textService.getMockText().then((res) => {
      this.text = res;
      this.words = this.text.split(' ');
    });
  }

  wordSelected(event) {
    if (this.textService.selectedElement === event.srcElement){
      this.textService.isWordSelected = !this.textService.isWordSelected;
    }
    else{
      this.textService.isWordSelected = true;
    }

    this.textService.selectedElement = event.srcElement;
    var word = this.removePunctuationMarks(this.textService.selectedElement.innerText.trim());

    this.textService.getSynonyms(word).subscribe(data => {
      this.synonyms = [];
      data.forEach(x => this.synonyms.push(x.word));
    })
  }

  clearSelection() {
    this.textService.isWordSelected = false;
    this.textService.selectedElement = null;
    this.synonyms = [];
  }

  replaceWord(event) {
    var replacer = event.srcElement.innerText.trim();
    replacer = replacer.slice(0, replacer.length -1);
    var word = this.textService.selectedElement.innerText.trim();
    
    this.textService.selectedElement.innerText = replacer + this.addEnding(word);
  }

  addEnding(word): String {
    for (let i = 0; i < this.punctiationMarks.length; i++){
      if (word.endsWith(this.punctiationMarks[i])){
        return this.punctiationMarks[i];
      }
    }
    return "";
  }

  removePunctuationMarks(word): String {
    for (let i = 0; i < this.punctiationMarks.length; i++){
      if (word.endsWith(this.punctiationMarks[i])){
        word = word.slice(0, word.length - 1);
      }
    }
    return word;
  }

  
}
