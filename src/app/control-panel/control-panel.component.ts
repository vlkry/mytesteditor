import { Component, OnInit } from '@angular/core';
import { TextService } from '../text-service/text.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  constructor(private textService: TextService) {
  }

  ngOnInit() {
  }

  applyStyle(style){
      if (this.textService.selectedElement.className.search(style) !== -1){
        this.textService.selectedElement.className = this.textService.selectedElement.className.replace(" " + style, '');
      }
      else {
        this.textService.selectedElement.className += " " + style;
      }
      
  }

  isApplied(style): Boolean{
    if (!this.textService.isWordSelected){
      return false;
    }
    else if (this.textService.selectedElement.className.search(style) !== -1){
      return true;
    } else {
      return false;
    }
  }
}
