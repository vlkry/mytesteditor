# PreRequisites

In order to run this App you need to have npm installed

# Steps to reproduce

    - run "npm install" command in terminal from the same folder you extracted the project
    - run "ng serve" command to start the application
    - navigate to http://localhost:4200/

# User's guide

1) Double click on the word to "select" it
2) After you select a word you are able to modify it by:
    a) Using top panel to make selected word Bold, Italic or Underlined
    b) Using right sidebar to replace your word with synonym (double click on synonym to repalce current selection)
3) Clicking away in the middle section will clear the selection
4) Enjoy 



